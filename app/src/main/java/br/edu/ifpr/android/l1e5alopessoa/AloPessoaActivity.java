package br.edu.ifpr.android.l1e5alopessoa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AloPessoaActivity extends AppCompatActivity  {

    public static final String DIGITE_UM_NOME = "Digite um nome";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alo_pessoa);

        Button botao_ok = (Button) findViewById(R.id.botao_ok);

        botao_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText pessoaEditText = (EditText) findViewById(R.id.editPessoa);
                String pessoa = pessoaEditText.getText().toString();

                TextView aloPessoaTextView = (TextView) findViewById(R.id.alo_pessoa);

                if(! "".equals(pessoa)) {
                    aloPessoaTextView.setText(alo_pessoa(pessoa));
                }
            }
        });

    }

    private String alo_pessoa(String pessoa){
        return "Alô, " + pessoa;
    }
}
